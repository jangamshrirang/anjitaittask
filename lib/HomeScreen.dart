import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
// import 'package:carousel_slider/carousel_slider.dart';
// import 'package:google_fonts/google_fonts.dart';
import 'package:tasktwo/model/storiesmodel.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HomeScreen extends StatefulWidget {
  // const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  // final String assetNam = 'assets/ic_play.svg';
  final Widget svg =
      SvgPicture.asset('assets/ic_play.svg', semanticsLabel: 'Acme Logo');
  final Widget svg2 =
      SvgPicture.asset('assets/star_premium.svg', semanticsLabel: 'Acme Logo');
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              // height: 300,
              child: Stack(
                children: [
                  Image.asset('assets/home-header.png'),
                  Positioned(top: 25, left: 20, child: svg2),
                  Positioned(
                      top: 55,
                      // left: ,
                      child: Image.asset(
                        'assets/cloud-header.png',
                        height:
                            // 150
                            0.19.sh,
                      )),
                ],
              ),
            ),
            Container(
                padding: EdgeInsets.all(15),
                width: double.infinity,
                height: 0.35.sh,
                child: imageSlider(context)),
            header2('Favourites', 'assets/ic_star.svg'),
            storylist(),
            header('EveryDay Stories',
                'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQMp1qo6Zhkmq5wwDPQHyQTKyjz3DNTFSdyng&usqp=CAU'),
            storylist(),
            header('Indian Mythological Tales',
                'http://4.bp.blogspot.com/_jHVOUHhuEy8/TKAlqMwuTTI/AAAAAAAAAB8/56gQw83-_bY/w1200-h630-p-k-no-nu/myth.jpg'),
            storylist(),
            header('ABC Phonics Stories',
                'https://i.ytimg.com/vi/IoCHarKoNfI/maxresdefault.jpg'),
            storylist(),
            header('Storyweaver',
                'https://storage.googleapis.com/story-weaver-e2e-production/ckeditor_assets/pictures/1002/content/storyweaver_summer.jpg'),
            storylist(),
            header('Animal Story',
                'https://parade.com/wp-content/uploads/2019/11/disney-plus-animated-movies-feature.jpg'),
            storylist(),
          ],
        ),
      ),
    );
  }

  storylist() {
    return Container(
      height: 200,
      // width: 200,
      child: ListView.builder(
          // shrinkWrap: true,
          itemCount: story.length,

          // physics: ScrollPhysics.vertical ,
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) {
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                // width: 200,
                height: 0.22.sh,
                child: Column(
                  children: [
                    Stack(
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(20),
                          child: Image.network(
                            story[index].img,
                            height: 0.2.sh,
                            width: 0.40.sw,
                            fit: BoxFit.fill,
                          ),
                        ),
                        Positioned(
                          // bottom: ,r
                          top: 110,
                          right: 60,
                          child: svg,
                          // Image.network(
                          //   'https://img.freepik.com/free-vector/play-button_53876-25521.jpg?size=626&ext=jpg',
                          //   height: 50,
                          //   width: 50,
                          // )
                        ),
                      ],
                    ),
                    Container(
                        child: Text(
                      story[index].name,
                      style: TextStyle(fontFamily: 'Poppins'),
                      // style: GoogleFonts.poppins(
                      //   // fontSize: 15,
                      // )
                    ))
                    // )
                  ],
                ),
              ),
            );
          }),
    );
  }

  header(String name, String img) {
    return Container(
      child: Row(
        children: [
          SizedBox(
            width: 10,
          ),
          ClipRRect(
            borderRadius: BorderRadius.circular(100),
            child: Image.network(
              img,
              // 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQMp1qo6Zhkmq5wwDPQHyQTKyjz3DNTFSdyng&usqp=CAU',
              height: 50,
              width: 50,
              fit: BoxFit.fill,
            ),
          ),
          SizedBox(
            width: 10,
          ),
          Text(
            name,
            // 'EveryDay Stories',
            style:
                //  GoogleFonts.poppins(fontSize: 18)
                TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Poppins'),
          ),
        ],
      ),
    );
  }

  header2(String name, String img) {
    return Container(
      child: Row(
        children: [
          SizedBox(
            width: 10,
          ),
          ClipRRect(
              borderRadius: BorderRadius.circular(100),
              child: SvgPicture.asset(
                img,
                semanticsLabel: 'Acme Logo',
                height: 50,
                width: 50,
                fit: BoxFit.fill,
              )),
          SizedBox(
            width: 10,
          ),
          Text(
            name,
            // 'EveryDay Stories',
            style:
                //  GoogleFonts.poppins(fontSize: 18)
                TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Poppins'),
          ),
        ],
      ),
    );
  }
}

Swiper imageSlider(context) {
  return new Swiper(
    autoplay: true,
    itemBuilder: (BuildContext context, int index) {
      return ClipRRect(
        borderRadius: BorderRadius.circular(30.0),
        child: new Image.network(
          "https://images.unsplash.com/photo-1595445364671-15205e6c380c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=764&q=80",
          fit: BoxFit.fill,
          // width: 300,
          // width: double.infinity,
          // height: 500,
        ),
      );
    },
    itemWidth: double.infinity,
    itemCount: 3,
    pagination: new SwiperPagination(),
    // viewportFraction: 0.8,
    scale: 0.9,
    // containerWidth: double.infinity,
    // curve: Curves.,

    // control: new SwiperControl(),
  );
}
