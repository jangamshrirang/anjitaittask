import 'package:flutter/material.dart';
import 'package:tasktwo/HomeScreen.dart';
import 'package:tasktwo/screen/signup.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'constant/app_style.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: Size(360, 690),
      // allowFontScaling: false,
      builder: () => MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter_ScreenUtil',
        theme: ThemeData(
            // primarySwatch: Colors.cyan,
            primaryColor: bgColor),
        home: SignUp(),
      ),
    );
  }
}
