import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

const Color bgColor = Color.fromRGBO(31, 125, 122, 1);
const Color white = Colors.white;
const Color divColor = Color(0xff818181);

const kStyleTitle = TextStyle(
  fontSize: 20,
  fontFamily: 'Lato',
  fontWeight: FontWeight.bold,
);

var smFont = TextStyle(fontSize: 16.sp);
var smFontWeight = TextStyle(fontSize: 16.sp, fontWeight: FontWeight.bold);
var sMedFont = TextStyle(fontSize: 17.sp);
var mFont = TextStyle(fontSize: 19.sp);
var fontWeight = TextStyle(
    fontSize: 16.sp,
    // color: Color(0xff818181),
    fontWeight: FontWeight.w500);
var lFont = TextStyle(fontSize: 25.sp, fontWeight: FontWeight.bold);
