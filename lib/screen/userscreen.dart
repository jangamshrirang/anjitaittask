import 'package:flutter/material.dart';
import 'package:tasktwo/model/usermodule.dart';
import 'package:tasktwo/service/networkrepo.dart';

class UserDetails extends StatefulWidget {
  

  @override
  _UserDetailsState createState() => _UserDetailsState();
}

class _UserDetailsState extends State<UserDetails> {
  Future<UserModule> _feeModel;

  @override
  Widget build(BuildContext context) {
    _feeModel = API_Manager().getUSerData();
 
    return Scaffold(
      appBar: AppBar(
        title: Text('User Details'),
      ),
      body: FutureBuilder<UserModule>(
        future: _feeModel,
        
        builder: (context, snapshot) {
         
          if (snapshot.hasData) {
            return ListView.builder(
              itemCount: snapshot.data.data.length,
              itemBuilder: (context, index) {
                return Card(
                  child: Column(
                    children: [
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        snapshot.data.data[index].name,
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      Text(snapshot.data.data[index].email),
                      Text(snapshot.data.data[index].gender),
                      SizedBox(
                        height: 10,
                      )
               
                    ],
                  ),
                );
              },
            );
          

          } else if (snapshot.hasError) {
            return Column(
              children: [
                Icon(
                  Icons.error_outline,
                  color: Colors.red,
                  size: 60,
                ),
                Center(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 16),
                    child: Text('Error: Data not Available'),
                  ),
                )
              ],
            );
            //   [

            // ];
          } else {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Center(
                  child: SizedBox(
                    child: CircularProgressIndicator(),
                    width: 60,
                    height: 60,
                  ),
                ),
                Center(
                  child: const Padding(
                    padding: EdgeInsets.only(top: 16),
                    child: Text('Awaiting result...'),
                  ),
                )
              ],
            );
            //   <Widget>[

            // ];
          }

          // );
        },
      ),
    );
  }
}
