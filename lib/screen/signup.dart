import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

// import 'package:rive/rive.dart';
import 'package:flutter/services.dart';
// import 'package:flare_flutter/flare_actor.dart';
// import './service/networkapirepo.dart';
// import 'loginpage.dart';
import 'package:http/http.dart' as http;
import 'package:tasktwo/HomeScreen.dart';
import 'package:tasktwo/screen/userscreen.dart';
import 'package:tasktwo/service/networkrepo.dart';
import '../constant/app_style.dart';
import 'package:email_validator/email_validator.dart';
// import 'package:flutter_dropdown/flutter_dropdown.dart';

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  TextEditingController userName = TextEditingController();
  TextEditingController userPass = TextEditingController();
  TextEditingController userPhone = TextEditingController();
  TextEditingController userEmail = TextEditingController();
  int message;
  String _dropDownValue;
  String token =
      '6e7a3cccb3e9c0deed4c00aa786e94139a25da0427b6cc8f79e3890955324963';
  final _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Task'),
          actions: [
            ElevatedButton(
                onPressed: () {
                  Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => UserDetails()));
                },
                child: Text('User List')),
            SizedBox(
              width: 0.01.sw,
            ),
            ElevatedButton(
                onPressed: () {
                  Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => HomeScreen()));
                },
                child: Text('Task 2'))
          ],
        ),
        key: _scaffoldKey,
        body: Form(
            key: _formKey,
            child: SingleChildScrollView(
              child: Column(
                  // mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: 0.25.sh,
                    ),
                    Center(
                      child: Container(
                        width: 0.68.sw,
                        decoration: BoxDecoration(),
                        child: TextFormField(
                          controller: userName,
                          maxLength: 100,
                          cursorColor: Colors.black,
                          keyboardType: TextInputType.emailAddress,
                          validator: (value) {
                            if (value.isEmpty || !(value.length > 3)) {
                              return 'username more than 3 Character long ';
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            hintStyle: smFont,
                            hintText: "Enter FirstName'",
                            border:
                                //  InputBorder(),
                                OutlineInputBorder(
                                    borderSide: new BorderSide(color: bgColor)),
                            focusedBorder: new OutlineInputBorder(
                                borderSide: new BorderSide(color: bgColor)),
                            // prefix: Icon(Icons.mail),
                            prefixIcon: Icon(
                              Icons.person,
                              color: bgColor,
                            ),
                            contentPadding: EdgeInsets.all(18),
                          ),
                        ),
                      ),
                    ),
                    Center(
                      child: Container(
                        width: 0.68.sw,
                        decoration: BoxDecoration(),
                        child: TextFormField(
                          controller: userEmail,
                          cursorColor: Colors.black,
                          keyboardType: TextInputType.emailAddress,
                          validator: (email) => EmailValidator.validate(email)
                              ? null
                              : "Invalid email address",
                          decoration: InputDecoration(
                            hintStyle: smFont,
                            hintText: "Enter Email ID",
                            border:
                                //  InputBorder(),
                                OutlineInputBorder(
                                    borderSide: new BorderSide(color: bgColor)),
                            // prefix: Icon(Icons.mail),
                            focusedBorder: new OutlineInputBorder(
                                borderSide: new BorderSide(color: bgColor)),
                            prefixIcon: Icon(
                              Icons.mail,
                              color: bgColor,
                            ),
                            contentPadding: EdgeInsets.all(18),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 0.01.sh,
                    ),
                    Container(
                      width: 0.5.sw,
                      child: DropdownButton(
                        hint: _dropDownValue == null
                            ? Text('Select Gender')
                            : Text(
                                _dropDownValue,
                                style: TextStyle(color: bgColor),
                              ),
                        isExpanded: true,
                        iconSize: 30.0,
                        style: TextStyle(color: bgColor),
                        items: [
                          'Male',
                          'Female',
                        ].map(
                          (val) {
                            return DropdownMenuItem<String>(
                              value: val,
                              child: Text(val),
                            );
                          },
                        ).toList(),
                        onChanged: (val) {
                          setState(
                            () {
                              _dropDownValue = val as String;
                            },
                          );
                        },
                      ),
                    ),
                    SizedBox(
                      height: 0.05.sw,
                    ),
                    ElevatedButton(
                        style: ElevatedButton.styleFrom(primary: bgColor),
                        onPressed: () async {
                          if (_formKey.currentState.validate()) {
                            var res = await API_Manager().signUp(
                                name: userName.text,
                                email: userEmail.text,
                                gender: _dropDownValue,
                                token: token);
                            setState(() {
                              message = res["data"]["id"];
                              // message = res["Milky"]["STATUS"]["MESSAGE"];
                              message == null
                                  ? Container
                                  : showDialog(
                                      context: context,
                                      builder: (context) => new AlertDialog(
                                        title: new Text('successful'),
                                        content: Text(message.toString()),
                                        actions: <Widget>[
                                          new FlatButton(
                                            onPressed: () {
                                              Navigator.of(context,
                                                      rootNavigator: true)
                                                  .pop(); // dismisses only the dialog and returns nothing
                                            },
                                            child: new Text('OK'),
                                          ),
                                        ],
                                      ),
                                    );
                            });
                          }
                        },
                        child: Text('Sign Up')),
                    SizedBox(
                      height: 0.03.sh,
                    ),
                    message == null
                        ? Container()
                        : Text(
                            message.toString(),
                            style: TextStyle(fontSize: 18),
                          )
                  ]),
            )));
  }
}
