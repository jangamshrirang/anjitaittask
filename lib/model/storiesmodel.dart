class StoriesModel {
  String name;
  String img;

  StoriesModel({this.name, this.img});
}

List<StoriesModel> story = [
  StoriesModel(
      name: 'Tiddalick',
      img:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcStn7NlfC2pmNr_jKJcixSZvv0Nqg3iVHwAPw&usqp=CAU'),
  StoriesModel(
      name: 'Kalam- Power In Unity',
      img:
          'https://i2.wp.com/myvoice.opindia.com/wp-content/uploads/sites/3/2020/10/kalam.jpg?fit=1200%2C675&ssl=1'),
  StoriesModel(
      name: 'TenaliRama',
      img: 'https://images-na.ssl-images-amazon.com/images/I/81k5DzxWvmL.jpg')
];
