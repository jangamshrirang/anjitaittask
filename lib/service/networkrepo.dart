

import 'package:http/http.dart' as http;

import 'dart:convert';

import 'package:tasktwo/model/usermodule.dart';

class API_Manager {
  Future signUp({
    String name,
    String gender,
    String email,
    String token,
  }) async {
    print(name);

    var url = Uri.parse('https://gorest.co.in/public-api/users');

    var response = await http.post(
      url,
      body: {
  
        'name': '$name',
        'gender': '$gender',
        'email': '$email',
        'status': 'Active',
      },
      headers: {
        // "Content-Type": "application/json",
        "Accept": "application/json",
        "Authorization": "Bearer " + token
      },
    );
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');
    var convertDatatoJson = jsonDecode(response.body);
    return convertDatatoJson;
  }



  Future<UserModule> getUSerData(
     ) async {
    var feeModel;
  

    try {
      var response =
          await http.get(Uri.parse("https://gorest.co.in/public-api/users"));
      
      if (response.statusCode == 200) {
        var jsonString = response.body;
        print(jsonString);
        var jsonMap = json.decode(jsonString);
        feeModel = UserModule.fromJson(jsonMap);
      }
    } catch (Execption) {
      return feeModel;
    }
    return feeModel;
  }
}
